const M = {
  bbox: [485000, 75000, 834000, 296000],
  data: {},
  dataSeries: [],
}

function main(){
  M.svg = d3.select("svg.choropleth")
  M.width = M.svg.attr('width')
  M.height = M.svg.attr('height')

  M.path = d3.geoPath()

  // Load the data
  // As we have 2 files to load, we need to load all of them before starting the processing.
  Promise.all([
    d3.json('data/vec200-topo.json'),
    d3.tsv('data/pop-fem-2034-2015.tsv', d3.autoType)
  ])
  .then(function (result) {
    M.topojson = result[0]
    M.data = result[1]

    // After loading the data, we can go ahead for some data preparation:

    // The data from the TSV file is available in M.data as an array. Later, we will need to access
    // the line from the TSV file for a given commune based on the ID of the commune. In an array,
    // retrieving an element based on the ID is inefficient; we need basically to search through all
    // elements until we find the one with the given ID. This requires a lot of iterations over the
    // array. We can modify the data structure and transform the array from the TSV file in an
    // object (dictionary) where the commune ID is the key and the value the element from the array.
    // This uses the fact that an object allows for rapid retrieval of an element based on a key
    // value.
    M.dataObj = {}
    M.data.forEach(d => {
      M.dataObj[d.id] = d
    })

    // We get out the data to show on the map in a simple flat array. This will be needed to
    // classify the values using Classybrew.
    M.dataSeries = M.data.map(d => parseFloat(d.p_fem_singl_2034))

    // Draw the map
    drawMap()
  })
}


function drawMap () {
  // The TopoJSON contains raw coordinates in CRS CH1903/LV03.
  // As this is already a projected CRS, we can use an SVG transform
  // to fit the map into the SVG view frame.
  // In a first step, we compute the transform parameters.

  // Compute the scale of the transform
  const scaleX = M.width / (M.bbox[2] - M.bbox[0])
  const scaleY = M.height / (M.bbox[3] - M.bbox[1])
  const scale = Math.min(scaleX, scaleY)

  const dx = -1 * scale * M.bbox[0]
  const dy = scale * M.bbox[1] + parseFloat(M.height)

  M.map = M.svg.append('g')
    .attr('class', 'map')
    .attr('transform', `matrix(${scale} 0 0 ${-1 * scale} ${dx} ${dy})`)

  // Compute the class limits using Jenks.
  // We use Classybrew to do this.
  M.brew = new classyBrew()
  M.brew.setSeries(M.dataSeries)
  M.brew.setNumClasses(6)
  M.brew.setColorCode('PuBu')
  M.breaks = M.brew.classify('jenks')

  M.color = d3.scaleThreshold()
    .domain(M.breaks.slice(1,6))
    .range(M.brew.getColors())

  // Communes are drawn first
  M.map
    .append('g').attr('class', 'communes')
    .selectAll('path')
    .data(topojson.feature(M.topojson, M.topojson.objects.communes).features)
    .join('path')
    .attr('fill', function (d) {
      return M.dataObj[d.properties.id]
        ? M.color(M.dataObj[d.properties.id].p_fem_singl_2034)
        : '#fff'
    })
    .attr('d', M.path)

  // Limits of the cantons
  // Due to our SVG transform above, stroke-width is in meters!
  M.map
    .append('g').attr('class', 'cantons')
    .selectAll('path')
    .data(topojson.feature(M.topojson, M.topojson.objects.cantons).features)
    .join('path')
    .attr('stroke', '#fff').attr('stroke-width', '200')
    .attr('fill', 'none').attr('d', M.path)

  // Lakes on top
  M.map
    .append('g').attr('class', 'lacs')
    .selectAll('path')
    .data(topojson.feature(M.topojson, M.topojson.objects.lacs).features)
    .join('path')
    .attr('fill', '#777').attr('d', M.path);
}
